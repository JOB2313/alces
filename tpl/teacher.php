<?php
/* this is not an entry point */
if (!defined("ALCES")) { exit("Not a valid entry point."); }

/* make a link for student's scope */
$link = makelink("student", $_GET["teacher"]);

/* teacher's page HTML body */
$content["body"] = <<<CNT_TEACHER
<p><strong>{$content["teacher_instructions_url"]}</strong></p>
<blockquote>$link</blockquote>
<p><strong>{$content["teacher_instructions_head"]}</strong></p>
<ol>
	<li>{$content["teacher_instructions_1"]}</li>
	<li>{$content["teacher_instructions_2"]}</li>
</ol>
CNT_TEACHER;

/* basic HTML template */
include "template.php";