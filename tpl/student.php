<?php
/* this is not an entry point */
if (!defined("ALCES")) { exit("Not a valid entry point."); }

/* extract poll settings for the text */
extract($settings, EXTR_PREFIX_ALL, "set");

/* get singular/plural */
$qword = $set_count > 1 ? $content["questions_pl"] : $content["questions_sg"];

/* student's page HTML body */
$content["body"] = <<<CNT_STUDENT
<p><strong>{$content["student_welcome"]}</strong></p>
<p>{$content["student_id"]} <code>$hash</code>. {$content["student_pick"]}
$set_count $qword {$content["from"]} $set_min
{$content["to"]} $set_max.</p>
<form method="post" action="?exam=$hash">
<button type="submit">{$content["student_button"]}</button>
</form>
CNT_STUDENT;

/* basic HTML template */
include "template.php";