<?php
/* this is not an entry point */
if (!defined("ALCES")) { exit("Not a valid entry point."); }

/* error page HTML body */
$content["body"] = <<<CNT_NOEXIST
<p><strong>{$content["noexist_id"]}</strong></p>
<p>{$content["noexist_instructions"]}</p>
CNT_NOEXIST;

/* basic HTML template */
include "template.php";